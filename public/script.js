"use strict";

document.addEventListener("DOMContentLoaded", function() {
  var listOfCards = document.querySelectorAll(".card"); // Search and filter

  var searchbox = document.querySelector(".searchbox");
  searchbox.addEventListener("keyup", function(e) {
    var research = e.target.value.toLowerCase();
    Array.from(listOfCards).forEach(function(card) {
      var title = card.lastElementChild.textContent;

      if (title.toLowerCase().indexOf(research) != -1) {
        card.style.display = "flex";
      } else {
        card.style.display = "none";
      }
    });
  }); // Menu

  var menuTrigger = document.querySelector(".menu-trigger");
  menuTrigger.addEventListener("click", function() {
    document
      .querySelector(".mobile-menu")
      .classList.toggle("mobile-menu--active");
    document.body.classList.toggle("overflow-hidden");
  }); // Card Check

  function isChecked() {
    if (document.querySelector(".card--active") != null) {
      document.querySelector(".card--active").classList.remove("card--active");
    }

    this.classList.toggle("card--active");
  }

  listOfCards = document.querySelectorAll(".card");

  for (var i = 0; i < listOfCards.length; i++) {
    listOfCards[i].addEventListener("click", isChecked, false);
  } // Tab switcher

  var tabs = document.querySelectorAll(".tab");

  function selectTab() {
    if (document.querySelector(".tab--active") != null) {
      document.querySelector(".tab--active").classList.remove("tab--active");
    }

    this.classList.toggle("tab--active");
  }

  for (var i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener("click", selectTab);
  }
});
