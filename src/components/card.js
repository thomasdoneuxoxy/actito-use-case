import React, { Component } from "react";

export class Card extends Component {
  static defaultProps = {
    title: "Template Default Name",
    imgUrl: "https://picsum.photos/200",
    id: ""
  };

  render() {
    return (
      <div className="card">
        <div className="card__img-container">
          <img src={this.props.imgUrl} alt="" className="card-img" />
        </div>
        <div className="card__text-container">{this.props.title}</div>
      </div>
    );
  }
}

export default Card;
