import React from "react";
import Store from "./icons/store";
import Dashboard from "./icons/dashboard";
import Portal from "./icons/portal";
import Profile from "./icons/profile";
import Sms from "./icons/sms";
import Form from "./icons/form";
import Print from "./icons/print";
import Workflow from "./icons/workflow";
import Datamart from "./icons/datamart";
import Mail from "./icons/mail";
import Logo from "./icons/logo";
import Check from "./icons/check";

const Icon = props => {
  switch (props.name) {
    case "store":
      return <Store {...props} />;
    case "logo":
      return <Logo {...props} />;
    case "check":
      return <Check {...props} />;
    case "dashboard":
      return <Dashboard {...props} />;
    case "portal":
      return <Portal {...props} />;
    case "profile":
      return <Profile {...props} />;
    case "sms":
      return <Sms {...props} />;
    case "form":
      return <Form {...props} />;
    case "print":
      return <Print {...props} />;
    case "workflow":
      return <Workflow {...props} />;
    case "datamart":
      return <Datamart {...props} />;
    case "mail":
      return <Mail {...props} />;
    default:
      return <div />;
  }
};
export default Icon;
