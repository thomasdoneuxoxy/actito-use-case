import React from "react";

const SVG = ({
  style = {},
  width = "100%",
  className = "",
  viewBox = "0 0 28 30"
}) => (
  <svg
    width="28px"
    height="30px"
    viewBox="0 0 28 30"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    className="menu-trigger"
  >
    <defs>
      <linearGradient
        x1="50.0748754%"
        y1="35.71704%"
        x2="50.7290497%"
        y2="80.6765353%"
        id="linearGradient-1"
      >
        <stop stopColor="#8CC63F" offset="15%"></stop>
        <stop stopColor="#7EAD40" offset="68%"></stop>
        <stop stopColor="#79A340" offset="100%"></stop>
      </linearGradient>
      <linearGradient
        x1="0%"
        y1="49.989555%"
        x2="99.9360859%"
        y2="49.989555%"
        id="linearGradient-2"
      >
        <stop stopColor="#8CC63F" offset="15%"></stop>
        <stop stopColor="#7EAD40" offset="68%"></stop>
        <stop stopColor="#79A340" offset="100%"></stop>
      </linearGradient>
    </defs>
    <g
      id="Symbols"
      stroke="none"
      strokeWidth="1"
      fill="none"
      fillRule="evenodd"
    >
      <g id="LAYOUT/NAV" transform="translate(-16.000000, -10.000000)">
        <g
          id="LAYOUT/Nav-components/Logo"
          transform="translate(16.000000, 10.000000)"
        >
          <g id="Actito_logo2019_RVB">
            <path
              d="M13.9199301,1.41062937 L13.8281818,1.41062937 C5.04615385,7.38573427 5.20671329,14.9406294 5.22678322,15.2273427 C5.22678322,10.4769214 9.07776054,6.62594406 13.8281818,6.62594406 C16.8374034,6.64098932 19.6331042,8.18299815 21.2511888,10.7202098 C22.0908571,12.0776677 22.5347321,13.6426498 22.5327972,15.2388112 L22.5327972,15.2388112 L22.5327972,27.4298601 C22.53285,27.6225423 22.6602359,27.7920007 22.8453147,27.8455944 L27.1918881,29.1157343 C27.3226195,29.1535905 27.4635428,29.1277672 27.5723568,29.0460157 C27.6811708,28.9642642 27.7452448,28.8361021 27.7452448,28.7 L27.7452448,15.2359441 C27.7452448,11.5692426 26.2886533,8.05271974 23.6959038,5.45997029 C21.1031544,2.86722084 17.5866315,1.41062937 13.9199301,1.41062937 Z"
              id="Path"
              fill="#79A340"
              fillRule="nonzero"
            ></path>
            <path
              d="M13.8281818,23.8488112 C9.07776054,23.8488112 5.22678322,19.9978339 5.22678322,15.2474126 C5.22678322,15.2474126 4.88559441,7.50615385 13.8281818,1.4220979 C6.21358374,1.46277251 0.0485727244,7.62139227 0,15.2359441 L0,15.2359441 C6.08692308,24.19 13.8281818,23.8488112 13.8281818,23.8488112 Z"
              id="Path"
              fill="url(#linearGradient-1)"
              fillRule="nonzero"
            ></path>
            <path
              d="M27.6993706,0.444405594 C27.7002387,0.307799643 27.6365809,0.17879737 27.5276335,0.0963812486 C27.4186862,0.0139651274 27.2772312,-0.0121956867 27.146014,0.0258041958 L22.7994406,1.29020979 C22.6143618,1.34380351 22.4869759,1.51326185 22.4869231,1.70594406 L22.5786713,15.1011888 C22.5786713,15.1011888 22.8252448,22.8768531 13.8740559,28.9695105 C21.3888112,28.9695105 27.793986,22.7363636 27.793986,15.1011888 L27.6993706,0.444405594 Z"
              id="Path"
              fill="#8CC63F"
              fillRule="nonzero"
            ></path>
            <path
              d="M0,15.2359441 C0,22.873986 6.23888112,28.9609091 13.8740559,28.9609091 C20.3251049,24.5283217 21.988042,19.3846853 22.4295804,16.7870629 C21.6214514,20.8958307 18.015662,23.8561837 13.8281818,23.8488112 L13.8281818,23.8488112 C13.8281818,23.8488112 6.08692308,24.19 0.00286713287,15.2474126"
              id="Path"
              fill="url(#linearGradient-2)"
              fillRule="nonzero"
            ></path>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default SVG;
