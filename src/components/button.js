import React, { Component } from "react";

export class button extends Component {
  static defaultProps = {
    copy: "button",
    className: ""
  };
  render() {
    return <div className={this.props.className}>{this.props.copy}</div>;
  }
}

export default button;
