import React, { Component } from "react";
import Card from "./components/card";
import Icon from "./components/icons";
import "./App.scss";
import MobileMenu from "./components/mobileMenu";
import Button from "./components/button";

class App extends Component {
  state = {
    cards: [
      {
        id: "1",
        title: "Japan Template",
        imgUrl: "./img/PNG/vignette_japon.png"
      },
      { id: "2", title: "Foodie's lover", imgUrl: "./img/PNG/burger.jpg" },
      { id: "3", title: "Hot Doggo", imgUrl: "./img/PNG/doggo.jpg" },
      { id: "4", title: "Yeaaaah", imgUrl: "./img/PNG/yeah.jpg" },
      { id: "5", title: "Coquillages et crustacés" },
      { id: "6" }
    ],
    displayCards: true,
    cardsScenar: [
      {
        id: "1",
        title: "Test"
      },
      { id: "2", title: "Test 2" },
      { id: "3", title: "Test 3" },
      { id: "4", title: "Test 4" },
      { id: "5", title: "Test 5" },
      { id: "6", title: "Test 6" }
    ],
    displayScenar: false
  };

  displayCard = () => {
    this.setState({
      displayCards: true,
      displayScenar: false
    });
  };
  displayCardScenar = () => {
    this.setState({
      displayCards: false,
      displayScenar: true
    });
  };

  render() {
    let cards = null;
    if (this.state.displayCards) {
      cards = (
        <div className="deck">
          {this.state.cards.map((card, index) => {
            return (
              <Card key={card.id} title={card.title} imgUrl={card.imgUrl} />
            );
          })}
        </div>
      );
    }

    let cardsScenar = null;
    if (this.state.displayScenar) {
      cardsScenar = (
        <div className="deck">
          {this.state.cardsScenar.map((card, index) => {
            return (
              <Card key={card.id} title={card.title} imgUrl={card.imgUrl} />
            );
          })}
        </div>
      );
    }

    return (
      <section className="app">
        <MobileMenu />
        <Icon width={38} name="logo" />
        <div className="aside-menu">
          <a href="https://www.actito.com" className="aside-menu__logo-link">
            <img
              src="./img/PNG/logo.png"
              alt="Actito's logo"
              className="aside-menu__actito-logo icon-img"
            />
          </a>
          <ul className="shortcuts">
            <li className="shortcuts__item">
              <a href="https://www.actito.com" className="icon-link">
                <Icon fill="#fff" width={22} name="dashboard" />
              </a>
            </li>
            <li className="shortcuts__item">
              <a href="https://www.actito.com" className="icon-link">
                <Icon fill="#fff" width={22} name="portal" />
              </a>
            </li>
            <li className="shortcuts__item">
              <a href="https://www.actito.com" className="icon-link">
                <Icon fill="#fff" width={22} name="profile" />
              </a>
            </li>
            <li className="shortcuts__item shortcuts__item--active">
              <a href="https://www.actito.com" className="icon-link">
                <Icon fill="#fff" width={22} name="mail" />
              </a>
            </li>
            <li className="shortcuts__item">
              <a href="https://www.actito.com" className="icon-link">
                <Icon fill="#fff" width={22} name="sms" />
              </a>
            </li>
            <li className="shortcuts__item">
              <a href="https://www.actito.com" className="icon-link">
                <Icon fill="#fff" width={22} name="form" />
              </a>
            </li>
            <li className="shortcuts__item">
              <a href="https://www.actito.com" className="icon-link">
                <Icon fill="#fff" width={22} name="print" />
              </a>
            </li>
            <li className="shortcuts__item">
              <a href="https://www.actito.com" className="icon-link">
                <Icon fill="#fff" width={22} name="workflow" />
              </a>
            </li>
            <li className="shortcuts__item">
              <a href="https://www.actito.com" className="icon-link">
                <Icon fill="#fff" width={22} name="datamart" />
              </a>
            </li>
          </ul>
          <a href="https://www.actito.com" className="aside-menu__store-link">
            <Icon fill="#fff" width={22} name="store" />
          </a>
        </div>
        <form className="actito-dashboard">
          <header className="top-bar">
            <ol className="top-bar__nav">
              <li>
                <h1 className="main-category">Campagnes e-mail</h1>
              </li>
              <li className="nav-item">Catalogue</li>
              <li className="nav-item">Gérer les campagnes e-mail</li>
              <li className="nav-item nav-item--active">
                Choisir le type de campagne
              </li>
            </ol>
            <a href="https://www.actito.com" className="top-bar__item">
              <img
                src="./img/PNG/documentation.png"
                alt="Documentation"
                className="icon-img--nav icon-img"
              />
            </a>
            <a
              href="https://www.actito.com"
              className="top-bar__item top-bar__item--center"
            >
              <img
                src="./img/PNG/notification.png"
                alt="Notification"
                className="icon-img--nav icon-img"
              />
            </a>
            <a href="https://www.actito.com" className="top-bar__item">
              <img
                src="./img/PNG/avatar.png"
                alt="Avatar"
                className="icon-img--nav icon-img"
              />
            </a>
          </header>
          <div className="campaign-selector">
            <div className="campaign-selector__tabs">
              <h2 className="tab tab--active" onClick={this.displayCard}>
                marketing
              </h2>
              <h2 className="tab" onClick={this.displayCardScenar}>
                scénarisée
              </h2>
              <h2 className="tab">transactionelle</h2>
            </div>
            <div className="campaign-selector__categories">
              <h3 className="subtitle">catégories de templates</h3>
              <ol className="list">
                <li className="list-item">Campagne classique</li>
                <li className="list-item">Campagne A/B test</li>
              </ol>
            </div>
            <section className="campaign-selector__group">
              <div className="campaign-selector__header">
                <h3 className="subtitle subtitle--small">
                  Créer une campagne classique de marketing de masse
                </h3>
                <input type="text" className="searchbox" />
              </div>
              <p className="simple-text">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto
                voluptatibus sunt accusantium repellat sequi hic reprehenderit
                atque <b>voluptatem</b> id voluptate fuga, dolores fugiat qui!
              </p>
              <div className="deck-wrapper">
                {cards}
                {cardsScenar}
              </div>
            </section>
          </div>
          <footer className="buttons-container">
            <Button copy="Annuler" className="button" />
            <Button copy="Valider" className="button button--dark" />
          </footer>
        </form>
      </section>
    );
  }
}

export default App;
